//
//  EmojiCollectionViewCell.swift
//  EmojiArt
//
//  Created by Olimjon Turgunov.
//  Copyright © 2017 Olimjon Turgunov. All rights reserved.
//

import UIKit

class EmojiCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var label: UILabel!
}
